# data_analysis

Collecting and analyzing demographic, economic and weather data for a geographic region to design plausible future scenarios that would affect residential electricity demand in Mexico.